#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

export DOCKER_BUILDKIT=1

#go test

docker build -t localhost:32000/authtest .
docker push localhost:32000/authtest

# make sure we are on the right cluster.
kubectl config use-context microk8s

kubectl -n ory delete -f authorization_api_local_deploy.yaml || true
sleep 5
kubectl -n ory apply -f authorization_api_local_deploy.yaml
