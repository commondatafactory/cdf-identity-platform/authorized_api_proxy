package main

import (
	"context"
	"fmt"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	acl "github.com/ory/keto/proto/ory/keto/relation_tuples/v1alpha2"
)

func newKetoClient() *acl.CheckServiceClient {
	conn, err := grpc.Dial("127.0.0.1:4466", grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		panic("Encountered error: " + err.Error())
	}

	checkClient := acl.NewCheckServiceClient(conn)
	readClient := acl.NewReadServiceClient(conn)
	expClient := acl.NewExpandServiceClient(conn)

	id := "8e90293b-e67c-44b0-839c-daa6760ed5d2" // stephan@preeker.net acc

	query := &acl.ListRelationTuplesRequest{
		Query: &acl.ListRelationTuplesRequest_Query{
			Namespace: "groups",
			Relation:  "access",
			// Object: "kvk_gm0848",
			Subject: acl.NewSubjectID(id),
		},
	}

	tuples, err := readClient.ListRelationTuples(context.Background(), query)

	if err != nil {
		log.Fatalf("Failed to retrieve tuples from Keto: %v", err)
	}

	for _, tuple := range tuples.RelationTuples {
		log.Printf("%+v \n", tuple)
	}

	check, err := (checkClient).Check(context.Background(), &acl.CheckRequest{
		Namespace: "permissions",
		Object:    "kvk_gm0848",
		Relation:  "access",
		// Subject:   &acl.Subject{Ref: &acl.Subject_Id{Id: id}},
		Subject: acl.NewSubjectID(id),
	})

	if err != nil {
		log.Fatalf("Failed to retrieve doing check from Keto: %v", err)
	}

	fmt.Printf("%+v \n", check.Allowed)

	xr := &acl.ExpandRequest{
		Subject:  acl.NewSubjectID("autobranche_demo_gm0848"),
		MaxDepth: 10,
	}

	expand_r, err := expClient.Expand(context.Background(), xr)

	if err != nil {
		log.Fatalf("Failed to expand request from Keto: %v", err)
	}

	fmt.Printf("%+v", expand_r)

	return &checkClient
}

func main() {
	newKetoClient()
}
