module gitlab.com/commondatafactory/authorization_api_proxy

go 1.22

toolchain go1.22.2

require (
	github.com/go-spatial/geom v0.1.0
	github.com/ory/client-go v1.0.2
	github.com/ory/keto/proto v0.11.1-alpha.0
	github.com/rs/cors v1.8.2
	github.com/sirupsen/logrus v1.9.3
	google.golang.org/grpc v1.52.3
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/oauth2 v0.2.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20230131230820-1c016267d619 // indirect
	google.golang.org/protobuf v1.29.0 // indirect
)
