/*
Set up a default logging to do audit logs of activities.
*/
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/go-spatial/geom/encoding/geojson"

	"github.com/sirupsen/logrus"
)

var auditLogger *logrus.Logger

func init() {
	logger := logrus.New()

	// Set JSON format
	logger.SetFormatter(&logrus.JSONFormatter{
		DisableTimestamp: true,
	})

	// Output to stdout instead of the default stderr
	// logger.SetOutput(os.Stdout)

	// Always include "audience": "audit"
	logger = logger.WithField("audience", "audit").Logger

	// Example log messages
	logger.Info("Application started")

	auditLogger = logger
}

// restore body after read.
func restoreBody(r *http.Request, bodyBytes []byte) {
	r.Body = io.NopCloser(bytes.NewReader(bodyBytes))
}

// extract geojson
func extractGeoJSON(r *http.Request) string {
	// Read and store the request body
	var bodyBytes []byte
	if r.Body != nil {
		var err error
		bodyBytes, err = io.ReadAll(r.Body)
		if err != nil {
			fmt.Println(err)
			return ""
		}
	}

	restoreBody(r, bodyBytes)
	// Parse form data (this consumes the body)
	// check for geojson geometry stuff.
	err := r.ParseForm()

	defer restoreBody(r, bodyBytes)

	if err != nil {
		fmt.Println(err)
		return ""
	}

	geometryS, geometryGiven := r.Form["geojson"]
	var geoinput geojson.Geometry
	if geometryGiven && geometryS[0] != "" {
		err := json.Unmarshal([]byte(geometryS[0]), &geoinput)
		if err != nil {
			log.Println("parsing geojson error")
			return ""
		}
		return geometryS[0]
	}
	return ""
}
