/*
Reverse Proxy with authorization to map-services like
tileserver and companion dataselection services.

Assumes  kratos-public, keto-read and authtileserver are in the same
namespace running on your k8s cluster

example permission 'kvk_gm0796' will allow a request to:

https://authorized.tiles.commondatafactory.nl/kvk.kvk_expand_sbi_gm0796.json
https://authorized.tiles.commondatafactory.nl/kvkds/gm_0796/list/
https://authorized.tiles.commondatafactory.nl/tiles/eigenaarstop7/acc.eigenaarstop7.json

- setup proxy services in init();
- start ListenAndServe requests
  - 1) validate incoming request
    2) validate session
    3) find active organisation, active role
    4) check if permissions is active
    5) find proxy matching with permissions
    6) forward proxy to correct tile dataset

- log audit messages if needed
*/
package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"github.com/rs/cors"
	"github.com/sirupsen/logrus"

	kratos "github.com/ory/client-go"
	acl "github.com/ory/keto/proto/ory/keto/relation_tuples/v1alpha2"
)

type RemoteServices map[string]string

// Internal API endpoints we are securing.
var remotes = RemoteServices{
	"kvktiles": "http://authtileserver:7800/",
	"kvkds":    "http://dataselectiekvk:8000/",
}

func (r RemoteServices) Get(name string) *httputil.ReverseProxy {
	urlstring := r[name]
	url, err := url.Parse(urlstring)

	if err != nil {
		panic(err)
	}

	proxy := httputil.NewSingleHostReverseProxy(url)
	proxy.Transport = clearResponseCORS{}
	return proxy
}

// Rewriting functions help create correct API calls to data providers with
// the correct dataselections allowed for by permissions.

// Allowed KvK parameters.
var ALLOWED_DATASELECTION_PARAMS = []string{
	// "any_match",
	"groupby",
	"reduce",
	"format",
	"sbicode",
	"l1_code",
	"l2_code",
	"l3_code",
	"l4_code",
	"l5_code",
	"id",
}

// RewriteKVKRequestURL need to do create a valid
// request to kvk dataselection API.
// We need to change the PATH and change the RawQuery
// from /kvkds/code to /
// NOTE permissions are already checked here.
func rewriteKVKRequestURL(organisations []string, req *http.Request) {
	// remove any existing illegal parameters.
	qold := req.URL.Query()
	q := url.Values{}

	// only copy allowed parameters from original request
	for param := range qold {
		for _, a := range ALLOWED_DATASELECTION_PARAMS {
			if strings.Contains(param, a) {
				values := qold[param]
				for _, v := range values {
					q.Add(param, v)
				}
			}
		}
	}
	// rewrite to /
	req.URL.Path = "/list/"

	for _, o := range organisations {
		org := strings.ToUpper(o)
		q.Add("match-gemeentecode", org) // GM0123
	}

	req.URL.RawQuery = q.Encode()
}

// NOTE permissions are already checked here
// Now just rewrite url to match specific data in tileserver using a postgres function
func rewriteTileRequestURL(organisations []string, req *http.Request) {

	// remove any existing illegal parameters.
	q := url.Values{}

	gmcodes := []string{}

	for _, o := range organisations {
		org := strings.ToUpper(o)
		gmcodes = append(gmcodes, org)

		log.Printf("add %s \n", org)
	}
	// org := strings.ToUpper(organisation)
	q.Add("gmcode", strings.Join(gmcodes, ",")) // GM0123, GM0123
	req.URL.RawQuery = q.Encode()
}

// newKratosClient to check the identity.
func newKratosClient() *kratos.APIClient {
	configuration := kratos.NewConfiguration()
	configuration.Servers = []kratos.ServerConfiguration{
		{
			URL: "http://kratos-public", // Kratos Admin API
		},
	}
	apiClient := kratos.NewAPIClient(configuration)

	return apiClient
}

// kvk.municipality_select/14/8432/5445.pbf?gmcode=GM0823
// kvk.municipality_select/14/8432/5445.pbf?gmcode=GM0823
// kvk.municipality_select/16/33732/21793.pbf?gmcode=GM0823

// newKetoClients to check for authorization
func newKetoClients() (*acl.CheckServiceClient, *acl.ReadServiceClient) {
	conn, err := grpc.Dial("keto-read:80", grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		panic("Encountered error: " + err.Error())
	}

	checkClient := acl.NewCheckServiceClient(conn)
	readClient := acl.NewReadServiceClient(conn)
	return &checkClient, &readClient
}

/*
Check with kratos if we received a valid session token in the request cookie

we validate a user checking for:

- cookie
- valid session
- user still active
- user has 2FA
*/
func activeSession(apiClient *kratos.APIClient, r *http.Request) (id string, err error) {

	var xSession string

	if cookie, err := r.Cookie("ory_kratos_session"); err == nil {
		xSession = cookie.Value
	} else {
		msg := fmt.Sprintf("missing session cookie: %s \n", r.URL.Path)
		fmt.Fprint(os.Stderr, msg)
		return "", errors.New(msg)
	}

	cookie := fmt.Sprintf("ory_kratos_session=%s", xSession)

	resp, kr, err := apiClient.FrontendApi.ToSession(context.Background()).Cookie(cookie).Execute()

	if err != nil {
		msg := fmt.Sprintf("Error when calling `FrontendApi.ToSession``: %v\n", err)
		fmt.Fprintf(os.Stderr, msg, err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", kr)
		return "", errors.New(msg)
	}

	// log what we get..
	if resp != nil {
		fmt.Fprintf(os.Stdout, "Traits  %v\n", resp.Identity.Traits)
		// fmt.Fprintf(os.Stdout, "ID  %v\n", resp.Identity.Id)
		fmt.Fprintf(os.Stdout, "Active  %v\n", resp.Active)
		fmt.Fprintf(os.Stdout, "ID %v\n", resp.Id)
	} else {
		fmt.Println("no kratos resp")
		return "", errors.New("no kratos repsonse")
	}

	if !(*resp.Active) {
		return "", errors.New("deactivated account")
	}

	// check if user is 2FA authenticated
	if *resp.AuthenticatorAssuranceLevel != "aal2" {
		msg := fmt.Sprintf("missing or is not 2FA authenticated current level = %s \n", *resp.AuthenticatorAssuranceLevel)
		return "", errors.New(msg)
	}

	return resp.Identity.Id, nil
}

/*
Check with keto for active organization(s)
*/
func activeOriganisations(subjectId string, readClient *acl.ReadServiceClient) ([]string, error) {

	objects := []string{}

	query := &acl.ListRelationTuplesRequest{
		Query: &acl.ListRelationTuplesRequest_Query{
			Namespace: "organizations",
			Relation:  "access",
			Subject:   acl.NewSubjectID(subjectId),
		},
	}

	tuples, err := (*readClient).ListRelationTuples(context.Background(), query)

	if err != nil {
		log.Printf("Failed to retrieve tuples from Keto: %v", err)
		return objects, err
	}

	if len(tuples.RelationTuples) == 0 {
		return objects, errors.New("no active groups")
	}

	for _, tuple := range tuples.RelationTuples {
		log.Printf("%+v \n", tuple)
		objects = append(objects, tuple.Object)
	}

	return objects, nil
}

type RewriteRequest func(organisation []string, req *http.Request)

// Object to match url path with needed permission and backend service
// to fetch authorized data from
// RewriteFunc is needed to sanitize url so you cannot access remote service
// with illegal parameters and access parts a user is not allowed to see.
type PathPermission struct {
	Path             string // path of url to match
	PermissionPrefix string // permission that is needed
	RemoteService    string // the k8s service to get data from.
	// Functions rewrites request to actually only select active
	// and thus authorized data
	RewriteFunc RewriteRequest // How to rewrite url and remove illegal parameters to ensure safe access.
	AuditLog    bool
}

func (pp PathPermission) Match(test string) bool {
	return strings.HasPrefix(test, pp.Path)
}

func (pp PathPermission) Sting() string {
	return fmt.Sprintf("%10s %10s", pp.Path, pp.PermissionPrefix)
}

/*
Permissions given list of organisations generate permissions
*/
type dataPermissions []string

func (pp *PathPermission) Permissions(organisations []string) dataPermissions {
	dataPermissions := []string{}

	for _, o := range organisations {
		permissionToCheck := pp.PermissionPrefix + o
		dataPermissions = append(dataPermissions, permissionToCheck)
	}
	return dataPermissions
}

var APIPaths = []PathPermission{
	{"/kvkds/", "kvk_", "kvkds", rewriteKVKRequestURL, true},
	{"/kvk.municipality_select", "kvk_", "kvktiles", rewriteTileRequestURL, false},
}

// Given url check which permission we need to check for.
func checkPermissions(id string, organisations []string, r *http.Request, checkClient *acl.CheckServiceClient) (
	access bool, dataPermissions []string, p PathPermission, err error) {

	dataPermissions = []string{}

	pp := PathPermission{}

	for _, ipp := range APIPaths {
		if ipp.Match(string(r.URL.Path)) {
			dataPermissions = ipp.Permissions(organisations)
			pp = ipp
			break
		}
	}

	if len(dataPermissions) == 0 {
		fmt.Println("no dataset selected mismatch in url?..", r.URL.Path)
		msg := fmt.Sprintf("no dataset selected options: %v \n", APIPaths)
		return false, dataPermissions, PathPermission{}, errors.New(msg)
	}

	// loop over all data permissions and check if we have access
	for _, oneDataPermission := range dataPermissions {

		checkTuple := &acl.CheckRequest{
			Namespace: "data-sources",
			Object:    oneDataPermission,
			Relation:  "access",
			Subject:   acl.NewSubjectID(id),
		}

		check, err := (*checkClient).Check(context.Background(), checkTuple)

		fmt.Printf("%+v", pp)

		if err != nil {
			fmt.Println(err)
			fmt.Printf("access failed %+v", checkTuple)
			return false, dataPermissions, pp, err
		}

		if !check.Allowed {
			log.Printf("no authorization for access %s \n", oneDataPermission)
			msg := fmt.Sprintf("failed keto check %+v", checkTuple)
			err = errors.New(msg)
			return false, dataPermissions, pp, err
		}
	}

	// all checks have passed
	fmt.Println("access ok to: ", dataPermissions)
	return true, dataPermissions, pp, nil
}

func main() {
	fmt.Println("starting authorized reverse proxy server")
	// StartSingleHTTPFrontend
	startKVKproxy()
}

/*
authorizedDataHandler

 1. validate cookie session
 2. find active organization
 3. validate permissions
 4. find proxy matching with permissions
 5. rewrite request to add selection parameters from active data permissions
 6. use api proxy to fetch selected and allowed data
*/
func authorizedDataHandler() func(http.ResponseWriter, *http.Request) {

	kratosClient := newKratosClient()
	checkKetoClient, readKetoClient := newKetoClients()

	return func(w http.ResponseWriter, r *http.Request) {

		// 1. validate user session on correct authorization,
		// returns identity id if valid else error.
		identity, err := activeSession(kratosClient, r)

		if err != nil {
			msg := fmt.Sprintf("no active session %v", err)
			http.Error(w, msg, http.StatusUnauthorized)
			log.Println(msg)
			return
		}

		// 2 find active organisations in keto role service
		organisations, err := activeOriganisations(identity, readKetoClient)

		if err != nil {
			msg := fmt.Sprintf("failed to find active organisation %v", err)
			http.Error(w, msg, http.StatusUnauthorized)
			log.Println(msg)
			auditLogger.WithFields(logrus.Fields{
				"identity": identity,
				"path":     r.URL.Path,
			}).Error("no active organisations for user")
			return
		}

		if len(organisations) > 1 {
			log.Println("NOTE: Multiple active organisations")
		}

		// 3, 4
		// check which service is being accessed and
		// validate user permission, if ok return remoteService
		access, _, pp, err := checkPermissions(identity, organisations, r, checkKetoClient)

		if err != nil {
			msg := fmt.Sprintf("error asking for permissions %v", err)
			http.Error(w, msg, http.StatusUnauthorized)
			return
		}

		if !access {
			http.Error(w, "no authorization", http.StatusUnauthorized)
			log.Println("no authorization..audit log created")
			auditLogger.WithFields(logrus.Fields{
				"audience":     "audit",
				"identity":     identity,
				"path":         r.URL.Path,
				"organisation": organisations,
			}).Error("no access for user for requested resource")
			return
		}

		// 5
		// All validation and permission checking is done here.
		// Now serve the API response.
		// find remoteservice and rewrite url of API call.
		p := remotes.Get(pp.RemoteService)
		// Rewrite request with organization information.
		originalDirector := p.Director
		originalPath := r.URL.Path
		p.Director = func(req *http.Request) {
			// sanitize url query paramaters
			pp.RewriteFunc(organisations, req)
			originalDirector(req)

		}

		// rewrite url back to original.
		defer func() {
			r.URL.Path = originalPath
		}()

		log.Println(r.URL.Path)

		// Do an audit log if needed.
		if pp.AuditLog {
			geojson := extractGeoJSON(r)
			q := r.URL.Query()
			// only log request which actually download data.
			// skip reduce requests.
			if _, ok := q["reduce"]; !ok {
				log.Println("data downloaded. audit log created")
				auditLogger.WithFields(logrus.Fields{
					"audience":     "audit",
					"identity":     identity,
					"path":         r.URL.String(),
					"originalPath": originalPath,
					"organisation": organisations,
					"geojson":      geojson,
				}).Info("authorized data downloaded")
			}
		}
		// Access is ok. Sercure API call is created now:
		// 6. Finally serve the authorized API response from API
		p.ServeHTTP(w, r)
	}
}

type clearResponseCORS struct{}

// pgtileserver sets cors access-control-allow-origin to *
// which is not so handy for a protected endpoint.
// remove it.
func (c clearResponseCORS) RoundTrip(r *http.Request) (*http.Response, error) {
	resp, err := http.DefaultTransport.RoundTrip(r)
	if err != nil {
		return nil, err
	}
	key := "Access-Control-Allow-Origin"
	delete(resp.Header, key)
	return resp, err
}

// startKVKproxy start a reverse proxy on 9090 using keto and kratos
// to verify access.
// example URL:
// http://authorized.tiles.commondatafactory.nl/kvk.kvk_expand_sbi_gm0796.json
// http://authorized.tiles.commondatafactory.nl/list/
func startKVKproxy() {

	c := cors.New(cors.Options{
		AllowedOrigins: []string{
			// acc environment
			"https://acc.dook.commondatafactory.nl",
			"https://acc.tvw.commondatafactory.nl",
			// production
			"https://dook.commondatafactory.nl",
			"https://demo.commondatafactory.nl",
			"https://dook.vng.nl",
			"https://dego.vng.nl",
			// test environment k8s local
			"http://map.vng.com",
			"http://authorized.data.vng.com",
		},
		AllowCredentials: true,
		// Enable Debugging for testing, consider disabling in production
		Debug: false,
	})

	// handle paths
	mux := http.NewServeMux()
	mux.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "OK")
	})

	mux.HandleFunc("/", authorizedDataHandler())

	// wrap mux with cors handler
	handler := c.Handler(mux)
	port := ":9090"
	fmt.Printf("running on %s\n", port)
	log.Fatal(http.ListenAndServe(port, handler))
}
