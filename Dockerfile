# STEP 1 build binary
FROM --platform=linux/amd64 golang:alpine3.21 AS builder

RUN apk update && apk add --no-cache git
RUN apk --no-cache add ca-certificates

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . /app/

# Fetch dependencies.
# RUN go get -d -v

# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux go build -o main

# STEP 2 build a small image
FROM scratch

# Copy static executable and certificates
COPY --from=builder /app/main /app/main
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

WORKDIR /app
# Run the binary.
EXPOSE 9090

ENTRYPOINT ["/app/main"]
