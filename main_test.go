package main

import (
	"fmt"
	"net/http"
	"strings"
	"testing"
)

// test rewriting of URL path and adding obligated GM parameter.
func TestRewriteKVKRequestURL(t *testing.T) {

	type testCase struct {
		path    string
		gmcodes []string
		expect  string
	}

	// only one GM code supported for now.
	// must implement must_any_match in lambda.
	mustmatch := "match-gemeentecode=GM1234"

	testCases := []testCase{
		{"http://kvkapi.nl/kvkds/gm_0123/list/",
			[]string{"gm1234"},
			mustmatch},
		{"http://kvkapi.nl/kvkds/list/?x=y",
			[]string{"gm1234"}, mustmatch},
		{"http://kvkapi.nl/kvkds/list/?any_match-l1_code=A",
			[]string{"gm1234"}, "any_match-l1_code=A&" + mustmatch},
		{"http://kvkapi.nl/kvkds/list/?any_match-l1_code=A&any_match-l2_code=B",
			[]string{"gm1234"}, "any_match-l1_code=A&any_match-l2_code=B&" + mustmatch},
	}

	for _, tc := range testCases {
		body := strings.NewReader(`{"empty": "body",}`)
		testRequest, _ := http.NewRequest("GET", tc.path, body)
		fmt.Println(testRequest.URL.Path)

		rewriteKVKRequestURL(tc.gmcodes, testRequest)

		if testRequest.URL.Path != "/list/" {
			t.Errorf("we got %s and not %s", testRequest.URL.Path, "/list/")
		}

		if strings.Compare(testRequest.URL.RawQuery, tc.expect) != 0 {
			t.Errorf("we got %s != %s", testRequest.URL.RawQuery, tc.expect)
		}
	}
}

// test rewriting of URL path and adding obligated GM parameter.
// the GM (gemeentecode) must be added to ensure access to only
// to the records allowed to see by municipality employee.
func TestRewriteTileRequestURL(t *testing.T) {

	type testCase struct {
		path    string
		gmcodes []string
		expect  string
	}

	rewriteTests := []testCase{
		{"/kvk.municipality_select/1/2/3.pbf", []string{"gm0123", "gm4321"}, "gmcode=GM0123%2CGM4321"},
	}

	expectedPath := "/kvk.municipality_select/1/2/3.pbf"

	for _, rt := range rewriteTests {
		body := strings.NewReader(`{"empty": "body",}`)
		url := fmt.Sprintf("http://kvkapi.nl%s", rt.path)
		testRequest, _ := http.NewRequest("GET", url, body)
		fmt.Println(testRequest.URL.Path)

		rewriteTileRequestURL(rt.gmcodes, testRequest)

		if !strings.HasPrefix(testRequest.URL.Path, expectedPath) {
			t.Errorf("rewritefailed %s and not %s", testRequest.URL.Path, expectedPath)
		}

		if testRequest.URL.RawQuery != rt.expect {
			t.Errorf("we got %s and not %s", testRequest.URL.RawQuery, rt.expect)
		}

	}

}
