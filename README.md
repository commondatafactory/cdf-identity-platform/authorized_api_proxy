AUTHORIZAION REVERSE PROXY API
------------------------------

Reverse Proxy with authorization to map-services like
tileserver and companion dataselection services.

Asumes  kratos-public, keto-read and authtileserver are in the same
namespace running on your k8s cluster

example permission 'kvk_gm0796' will allow a request to:

http://authorized.tiles.commondatafactory.nl/kvk.kvk_expand_sbi_gm0796.json
http://authorized.tiles.commondatafactory.nl/kvkds/gm_0796/list/
http://authorized.tiles.commondatafactory.nl/tiles/eigenaarstop7/acc.eigenaarstop7.json


Configuration
-------------


NOTHING to configure the authorzation proxy

reverse proxy listens to port 9090

authorization service asumes precense of in the deployed namespace

	`keto-read`, `kratos-admin`, `authtileservice`

services are running in the kubernetes cluster
to check for session and authorization


Current Permissions
-----------

	`gm0796_kvk`
	`gm0848_kvk`
	`gm1926_kvk`
	`gm0861_kvk`
        `gm0441_kvk` Schagen
